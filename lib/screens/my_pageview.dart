import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:trello_task/bloc/business.dart';
import 'package:trello_task/model/task.dart';
import 'package:trello_task/widgets/viewpage.dart';

class MyPageView extends StatefulWidget {
  @override
  _MyPageViewState createState() => _MyPageViewState();
}

class _MyPageViewState extends State<MyPageView> {
  var _controller = PageController(viewportFraction: 0.7, initialPage: 1);
  Map<String, List<Task>> currentMap;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<Map<String, List<Task>>>(
      stream: Business().todoMapStream,
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return LinearProgressIndicator();
        } else {
          currentMap = snapshot.data;
          return PageView(
            controller: _controller,
            dragStartBehavior: DragStartBehavior.down,
            children: <Widget>[
              Center(),
              for (int i = 0; i < currentMap.length; i++)
                Viewpage(
                  tasks: currentMap.values.toList()[i],
                  heading: currentMap.keys.toList()[i],
                  onWillAccept: (_) {
                    print('hovering over $i');
                    _controller.animateToPage(
                      i + 1,
                      duration: Duration(milliseconds: 400),
                      curve: Curves.easeIn,
                    );
                    return false;
                  },
                  onAccept: (_) {
                    print("This should not accept here");
                  },
                  onLeave: (_) {
                    print('The hover left he block');
                  },
                ),
            ],
          );
        }
      },
    );

  }
}