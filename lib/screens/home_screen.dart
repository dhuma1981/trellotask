import 'package:flutter/material.dart';
import 'package:trello_task/screens/my_pageview.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Trell Task"),),
      body: MyPageView(),
    );
  }
}