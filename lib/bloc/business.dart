import 'package:rxdart/rxdart.dart';
import 'package:trello_task/model/task.dart';

class Target {
  final String listTitle;
  final int position;

  Target(this.listTitle, this.position);
}

class Business {
  static final Business _singleton = Business._internal();

  factory Business() => _singleton;

  Business._internal();

  BehaviorSubject<Map<String, List<Task>>> _subject = BehaviorSubject.seeded({
    'TO DO': [
      Task('Go to Home'),
      Task('Go to Office'),
      Task('Go to Party'),
      Task('Got To Lunch'),
    ],
    'DOING': [
      Task('Working'),
      Task('Internt Surfing'),
      Task('Reading'),
    ],
    'DONE': [
      Task('Breakfast'),
      Task('Lunch'),
      Task('Dinner'),
    ]
  });

  Stream<Map<String, List<Task>>> get todoMapStream => _subject.stream;

  addAList(String listTitle) async {
    final currentMap = await _subject.last;
    currentMap.addAll({listTitle: []});

    _subject.add(currentMap);
  }

  removeAList(String listTitle) async {
    final currentMap = await _subject.last;
    currentMap.remove(listTitle);
    _subject.add(currentMap);
  }

  addATaskToList(String listTitle, Task task) async {
    final currentMap = await _subject.last;
    currentMap[listTitle].add(task);
    _subject.add(currentMap);
  }

  moveFromSourceToDestination(
      Target sourceTarget, Target destinationTarget) async {
    print("moving source ");
    final currentMap = await _subject.first;
    // take the task from source target
    final currentTask =
        currentMap[sourceTarget.listTitle][sourceTarget.position];
    // remove the task from source target
    currentMap[sourceTarget.listTitle].removeAt(sourceTarget.position);
    // put the task in destination target
    currentMap[destinationTarget.listTitle].insert(
        sourceTarget.position < destinationTarget.position
            ? destinationTarget.position - 1
            : destinationTarget.position,
        currentTask);

    // update the subject
    _subject.add(currentMap);
    print("moved boxes");
  }

  dispose() {
    _subject.close();
  }
}
