import 'package:flutter/material.dart';
import 'package:trello_task/widgets/drop_target.dart';

class DraggableCard extends StatelessWidget {
  final String description;
  final DropTargetData dropTargetData;

  DraggableCard(this.description, this.dropTargetData);

  @override
  Widget build(BuildContext context) {
    return LongPressDraggable<DropTargetData>(
      child: Card(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 18),
          child: Center(
            child: Text(description),
          ),
        ),
      ),
      feedback: Card(
        color: Colors.grey[300],
        child: SizedBox(
          width: MediaQuery.of(context).size.width * 0.55,
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Center(
              child: Text(description),
            ),
          ),
        ),
      ),
      childWhenDragging: Container(),
      data: dropTargetData,
    );
  }
}
