import 'package:flutter/material.dart';
import 'package:trello_task/bloc/business.dart';
import 'package:trello_task/model/task.dart';
import 'package:trello_task/widgets/draggble_card.dart';
import 'package:trello_task/widgets/drop_target.dart';

class Viewpage extends StatelessWidget {
  final List<Task> tasks;
  final String heading;
  final Function(DropTargetData) onAccept;
  final bool Function(DropTargetData) onWillAccept;
  final Function(DropTargetData) onLeave;

  const Viewpage(
      {Key key,
      this.tasks,
      this.heading,
      this.onAccept,
      this.onWillAccept,
      this.onLeave})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DragTarget<DropTargetData>(
      builder: (_, __, ___) {
        return Padding(
          padding: const EdgeInsets.all(15.0),
          child: Container(
            decoration:
                BoxDecoration(border: Border.all(), color: Colors.greenAccent),
            padding: EdgeInsets.all(5),
            child: Column(
              children: <Widget>[
                Text(
                  heading,
                  style: Theme.of(context).textTheme.headline,
                ),
                Expanded(
                  child: ListView.separated(
                    itemCount: tasks.length + 2,
                    separatorBuilder: (context, index) {
                      return DropTarget(heading, index);
                    },
                    itemBuilder: (context, index) {
                      if (index < 1 || index == tasks.length + 1) {
                        return SizedBox();
                      } else {
                        return DraggableCard(
                          tasks[index - 1].description,
                          DropTargetData(
                              sourceTarget: Target(
                                heading,
                                index - 1,
                              ),
                              task: null),
                        );
                      }
                    },
                  ),
                )
              ],
            ),
          ),
        );
      },
      onWillAccept: onWillAccept,
      onAccept: onAccept,
      onLeave: onLeave,
    );
  }
}
