import 'package:flutter/material.dart';
import 'package:trello_task/bloc/business.dart';
import 'package:trello_task/model/task.dart';

class DropTargetData {
  final Target sourceTarget;
  final Task task;

  DropTargetData({
    @required this.sourceTarget,
    @required this.task,
  });
}

class DropTarget extends StatefulWidget {
  final String title;
  final int position;

  DropTarget(this.title, this.position);

  @override
  _DropTargetState createState() => _DropTargetState();
}

class _DropTargetState extends State<DropTarget> {
  String get listtitle => widget.title;

  int get position => widget.position;

  Color containerColor;

  @override
  void initState() {
    super.initState();
    containerColor = Colors.white;
  }

  @override
  Widget build(BuildContext context) {
    return DragTarget(
      builder: (context, candidateData, rejectedData) {
        return Container(
          height: 30,
        );
      },
      onAccept: (DropTargetData dropTargetData) {
        print(dropTargetData.sourceTarget.position);
        print(position);

        Business().moveFromSourceToDestination(
            dropTargetData.sourceTarget, Target(listtitle, position));
        setState(() {
          containerColor = Colors.white;
        });
      },
      onWillAccept: (DropTargetData dropTargetData) {
        setState(() {
          containerColor = Colors.green;
        });

        return true;
      },
      onLeave: (DropTargetData dropTargetData) {
        setState(() {
          containerColor = Colors.red;
        });
      },
    );
  }
}
