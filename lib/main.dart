import 'package:flutter/material.dart';
import 'package:trello_task/screens/home_screen.dart';


void main() => runApp(
  MaterialApp(
    title: "Trello Task",
    home: HomeScreen(),
  )
);

